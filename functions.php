<?php

require_once("config.php");
require_once("manager.php");
require_once("result.php");

require 'vendor/autoload.php';

function process_rows($rows)
{
	$filesManager = new FilesListManager($_SERVER["DOCUMENT_ROOT"] . "/files.txt", Config::get("PHOTO_PATH"));
	$filesManager->loadList();

	if ($filesManager->getUpdatedAt() < time() - 86400)
		$filesManager->update();

	$files = $filesManager->getList();

	foreach ($rows as &$row)
	{
		$match = preg_grep("/." . $row . "./", $files);

		$row = [
			"ART" => $row,
			"FILES" => (array)$match
		];
	}

	Result::set($rows);

	return true;
}

function process_uploaded_file($file_name)
{
	$info = new SplFileInfo($file_name);

	if (!$info->isFile()) {
		die("File not found");
	}

	$ext = $info->getExtension();
	$fn = $info->getFilename();

	$oldEncoding = false;

	if (function_exists("mb_internal_encoding")) 
	{
		$oldEncoding = mb_internal_encoding();
		mb_internal_encoding('latin1');
		if ($ext == "xls") {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
		} elseif ($ext == "xlsx") {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		}
		$reader->setReadDataOnly(true);

		$spreadsheet = $reader->load($file_name);
	}

	if ($oldEncoding)
		mb_internal_encoding($oldEncoding);

	$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

	$tpl = "/(\d{2}-(\p{L})?\d{4})(\/\d)*(\*)?/u";

	$curRow = 0;
	$rows = array();

	foreach ($sheetData as $k => $row) {
		if ($row["B"] == $curRow + 1 && $row["B"] + 1 != $row["C"]) 
		{
			if (preg_match($tpl, $row["C"], $matches))
				$rows[$matches[1]]++;

			$curRow = $row["B"];
		} 
		elseif (preg_match($tpl, $row["B"], $matches)) 
		{
			$rows[$matches[1]]++;
		}
	}

	$rows = array_keys($rows);

	return process_rows($rows);
}

function pack_files($type = false, $filter = false)
{
	// $z_name = $_SERVER["DOCUMENT_ROOT"] . Config::get("STORE_PATH") . DIRECTORY_SEPARATOR . Config::get("ZIP_NAME");
	// $files = [];

	// $rows = 

	// if ($files) 
	// {
	// 	$zip = new ZipArchive();
	// 	$zip->open($z_name, ZipArchive::CREATE | ZipArchive::OVERWRITE);
	// 	foreach ($files as $name => $file) {
	// 		if (file_exists($file)) {
	// 			$zip->addFile($file, str_replace("--", "-к", $name));
	// 		}
	// 	}
	// 	$zip->close();
	// 	return $z_name;
	// }
	// return false;
}

function delete_files()
{
	$pattern = $_SERVER["DOCUMENT_ROOT"] . Config::get("STORE_PATH") . DIRECTORY_SEPARATOR . "*";

	foreach (glob($pattern, GLOB_BRACE) as $k => $v) 
	{
		if (is_file($v))
			unlink($v);
		else {
			$filesIterator = new RecursiveIteratorIterator(
				(new RecursiveDirectoryIterator($v, FilesystemIterator::SKIP_DOTS)),
				RecursiveIteratorIterator::CHILD_FIRST
			);

			foreach ($filesIterator as $file) {
				$file->isDir() ?  rmdir($file) : unlink($file);
			}
		}
	}
}