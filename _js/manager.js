// функции для кабинета менеджера
$(document).ready(function () {
    $('.changeUserActive').click(function(){
        var user_active = $(this).attr('data-user-active');

        var user_id = $(this).attr('data-user-id');
        if (user_active == 'Y'){
            if(confirm("Вы действительно хотите деактивировать этого пользователя?")) {
                user_active = 'N';
            } else {
                return false;
            }
        } else {
            if (confirm("Вы действительно хотите активировать этого пользователя?")){
                user_active = 'Y';
            } else{
                return false;
            }
        }
        $.ajax({
            type: "POST",
            url: "/ajax.php",
            data: {
                type: 'set_user_active',
                data: {
                    user_id: user_id,
                    user_active: user_active
                }
            },
            success: function(){
                location.reload();
            }
        });
    });
});