$(document).ready(function () {

    function LastErrorFocus(){
        var firstError = $('.registrationForm').find('td.error').eq(0).find('input, textarea');
        $('html, body').animate(
            {scrollTop: firstError.offset().top - 10},
            500
        );
        firstError.focus();
    }

    // маска телефона
    var maskList = $.masksSort($.masksLoad("/_js/plugins/phones-ru.min.json"), ['#'], /[0-9]|#/, "mask");
    var maskOpts = {
        inputmask: {
            definitions: {
                '#': {
                    validator: "[0-9]",
                    cardinality: 1
                }
            },
            //clearIncomplete: true,
            showMaskOnHover: false,
            autoUnmask: true
        },
        match: /[0-9]/,
        replace: '#',
        list: maskList,
        listKey: "mask",
        onMaskChange: function(maskObj, completed) {
            if (completed) {
                var hint = maskObj.name_ru;
                if (maskObj.desc_ru && maskObj.desc_ru != "") {
                    hint += " (" + maskObj.desc_ru + ")";
                }
            }
            $(this).attr("placeholder", $(this).inputmask("getemptymask").join(''));
        }
    };

    $("input[name=phone]").inputmasks(maskOpts);


    $('body').on('submit', '.registrationForm', function (event) {
        event.preventDefault();
        $(this).find('.error').removeClass('error');
        // получаем тип пользователя
        var userType = $(this).find("input[name=user-type]").val();
        var isOpt = userType == 'opt';
        var isRetail = userType == 'retail';
        if (!isOpt && !isRetail) return false;
        // валидация
        var info = $(this).serializeObject();
        var error = false;
        var err = '';
        var emailPattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
        //общие проверки для оптовика и розничного покупателя
        if (info.name.length < 5) {
            error = true;
            err = 'Слишком короткое имя';
            $(this).find('#name').parent().addClass('error').find('.errorText').html(err);
        }
        if (info.mode == 'reg') {
            if (info.email.length == 0) {
                error = true;
                err = 'Поле не заполнено';
                $(this).find('#email').parent().addClass('error').find('.errorText').html(err);
            } else if (!emailPattern.test(info.email)){
                error = true;
                err = 'Электронная почта заполнена неверно';
                $(this).find('#email').parent().addClass('error').find('.errorText').html(err);
            }
        }
        if (info.phone.length == 0) {
            error = true;
            err = 'Поле не заполнено';
            $(this).find('#phone').parent().addClass('error').find('.errorText').html(err);
        } else if ($('#phone').get(0).value.indexOf('_') >= 0) {
            error = true;
            err = 'Телефон введён не полностью';
            $(this).find('#phone').parent().addClass('error').find('.errorText').html(err);
        }
        // уникальные проверки только для оптовика
        if (isOpt){
            if (info.company.length < 5) {
                error = true;
                err = 'Слишком короткое название';
                $(this).find('#company').parent().addClass('error').find('.errorText').html(err);
            }
            if (info.juridicalAdress.length < 10) {
                error = true;
                err = 'Слишком короткий адрес';
                $(this).find('#juridicalAdress').parent().addClass('error').find('.errorText').html(err);
            }
            if (info.actualAdress.length < 10) {
                error = true;
                err = 'Слишком короткий адрес';
                $(this).find('#actualAdress').parent().addClass('error').find('.errorText').html(err);
            }
            if (info.companyDetails.length < 10) {
                error = true;
                err = 'Вероятно, поле заполнено не полностью';
                $(this).find('#companyDetails').parent().addClass('error').find('.errorText').html(err);
            }
        }
        // разбираем ошибки или отправляем
        if (error){
            LastErrorFocus();
        } else {
            var t;
            if (info.mode == 'reg') {
                t = 'registration';
            } else {
                t = 'edit_data'
            }
            $.ajax({
                type: "POST",
                url: "/ajax.php",
                data: {
                    type: t,
                    data: info
                },
                success: function(msg){
                    if (msg === 'modal')
                    {
                        //модал для перезвона
                        var regSuccessModal = new jBox('Modal', {
                            closeOnClick: 'overlay',
                            closeButton: 'box',
                            addClass: 'recallForm',
                            onOpen: function () {
                                blur(true);
                                window.setTimeout('$("input[name=numberPhone]").inputmask("+7(999)999-99-99")', 500);
                            },
                            onClose: function () {
                                window.location.replace('/catalog/');
                            },
                            ajax: {
                                type: "POST",
                                url: '/ajax.php',
                                data: {
                                    type: 'regSuccessModal'
                                },
                                reload: false
                            },
                        });
                        regSuccessModal.open();
                    }
                    else if (msg === 'redirect')
                        window.location.replace('/cabinet/');
                    else if (msg === 'ok') {
                        if (info.mode == 'reg') {
                            $('.beforeRefistration').fadeOut();
                            $('.afterRegistration').fadeIn();
                        } else {
                            location.reload(true);
                        }
                    } else {
                        var serverError = JSON.parse(msg);
                        err = serverError.errorText;
                        $('.registrationForm').find('#email').parent().addClass('error').find('.errorText').html(err);
                        LastErrorFocus();
                    }
                }
            });
        }
    });

    $('body').on('submit', '.changePassForm', function (event) {
        event.preventDefault();
        var info = $(this).serializeObject();
        $.ajax({
            type: "POST",
            url: "/ajax.php",
            data: {
                type: 'editPassword',
                data: info
            },
            success: function (msg) {
                if (msg === 'ok') {
                    $('.changePassForm td').html('').eq(0).html('Пароль успешно изменен')
                } else {
                    var serverError = JSON.parse(msg);
                    err = serverError.errorText;
                    if (err == 'Введённый пароль не совпадает с текущим') {
                        $('.changePassForm').find('#oldPass').parent().addClass('error').find('.errorText').html(err);
                    } else {
                        $('.changePassForm').find('#newPass').parent().addClass('error').find('.errorText').html(err);
                    }
                }
            }
        });
    });

    //снимаем error по клику в регистрации
    $('body').on('click change keyup', '.registrationForm .error, .changePassForm .error', function(){
        $(this).removeClass('error');
        $(this).find('.errorText').html('');
    });

    $('.button.but1, .button.but2').click(function () {
        $('.startBlock').hide();
        $('.tabs').show();
        if ($(this).hasClass('but1')) {
            $('.historyBlock').show();
            $('.tabs .tabButton').eq(0).addClass('active');
        } else {
            $('.dataBlock').show();
            $('.tabs .tabButton').eq(1).addClass('active');
        }
    });

    $('.tabs .tabButton').click(function () {
        $('.blockBlock').hide().eq($(this).index()).show();
    });

});