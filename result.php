<?php

class Result
{
    public static function set(array $data)
    {   
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/result", serialize($data));
    }

    public static function get()
    {
        $path = $_SERVER["DOCUMENT_ROOT"] . "/upload/result";

        if (file_exists($path))
            return unserialize(file_get_contents($path));
        else
            return [];
    }
}