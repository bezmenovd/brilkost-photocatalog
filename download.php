<?php


require("functions.php");

$mime_types = array(
	"jpeg" => "JPG",
	"tif" => "TIF"
);

$type = ($_REQUEST["format"] && isset($mime_types[$_REQUEST["format"]]) ? $mime_types[$_REQUEST["format"]] : false);
$filter = ($_REQUEST["card"] ?$_REQUEST["card"] : false);

$zip = pack_files($type, $filter);

if ($zip && file_exists($zip)) 
{
	header("Location: " . str_replace($_SERVER["DOCUMENT_ROOT"], "", $zip) , true, 303);

	if (ob_get_level()) {
		ob_end_clean();
	}
	
	$filename = basename($zip);
	header("X-Sendfile: " . $filename);
	header('Content-Type: application/zip');
	header('Content-Disposition: attachment; filename="'.basename($zip).'"');
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Length: ' . filesize($zip));
	$handle = fopen($zip, "rb");
	while (!feof($handle)){
		//I would suggest to do some checking
		//to see if the user is still downloading or if they closed the connection
		echo fread($handle, 8192);
	}
	fclose($handle);
	
}

?>